Thanks for downloading this theme!

Theme Name: TheEvent
Theme URL: https://bootstrapmade.com/theevent-conference-event-bootstrap-template/
Author: BootstrapMade.com
Author URL: https://bootstrapmade.com

#####

Para agregar speakers:

En main.js, agregar:

```
.when("/nombre-del-speaker", {
    templateUrl: "speakers/nombre-del-speaker.html"
})
```

y crear el archivo duplicando el de otro speaker.