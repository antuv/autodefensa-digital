var app = angular.module('app', ['ngRoute']);

app.config(function ($routeProvider) {
	$routeProvider
		.when("/", {
			templateUrl: "home.html"
		})
		.when("/alvarez", {
			templateUrl: "speakers/alvarez.html"
		})
		.when("/aybar", {
			templateUrl: "speakers/aybar.html"
		})
		.when("/bustos", {
			templateUrl: "speakers/bustos.html"
		})
		.when("/punch", {
			templateUrl: "speakers/punch.html"
		})
		.when("/wrobel", {
			templateUrl: "speakers/wrobel.html"
		})
		.when("/pereira", {
			templateUrl: "speakers/pereira.html"
		})
		.when("/martinez-quaranta-suarez", {
			templateUrl: "speakers/martinez-quaranta-suarez.html"
		})
		.when("/schmitman", {
			templateUrl: "speakers/schmitman.html"
		})
		.when("/escobar", {
			templateUrl: "speakers/escobar.html"
		})
		.when("/toledo", {
			templateUrl: "speakers/toledo.html"
		})
		.otherwise({
			redirectTo: "/"
		});
});

app.controller(
	"AppController",
	function AppController($scope, $location, $route) {
		// When the location changes, capture the state of the full URL.
		$scope.$on(
			"$locationChangeSuccess",
			function locationChanged() {
				var loc = $location.url();
				if (loc.substring(1,2) === '#') {
					$('a[href=' + loc.substring(1) + ']').parent().addClass('menu-active');
					setTimeout(function() {
						$('html, body').animate({
							scrollTop: $(loc.substring(1)).offset().top - getTopSpace()
						}, 1500, 'easeInOutExpo');
					}, 100);
				} else if ($location.url() !== '/') {
					$('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
					$('.menu-active').removeClass('menu-active');
				}
				setTimeout(() => {
					if ($('.venobox').length) {
						$('.venobox').venobox({
							bgcolor: '',
							overlayColor: 'rgba(6, 12, 34, 0.85)',
							closeBackground: '',
							closeColor: '#fff'
						});
					}
				}, 500);
			}
		);
		$scope.scrollTo = function (hash) {
			$('html, body').animate({
				scrollTop: $('#' + hash).offset().top - getTopSpace()
			}, 1500, 'easeInOutExpo');
		};
	}
);

function getTopSpace() {
	var top_space = 0;
	if ($('#header').length) {
		top_space = $('#header').outerHeight();
		if (!$('#header').hasClass('header-fixed')) {
			top_space = top_space - 20;
		}
	}
	return top_space;
}

jQuery(document).ready(function( $ ) {

	// Back to top button
	$(window).scroll(function() {
		if ($(this).scrollTop() > 100) {
			$('.back-to-top').fadeIn('slow');
		} else {
			$('.back-to-top').fadeOut('slow');
		}
	});
	$('.back-to-top').click(function(){
		$('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
		return false;
	});

	// Header fixed on scroll
	$(window).scroll(function() {
		if ($(this).scrollTop() > 100) {
			$('#header').addClass('header-scrolled');
		} else {
			$('#header').removeClass('header-scrolled');
		}
	});

	if ($(window).scrollTop() > 100) {
		$('#header').addClass('header-scrolled');
	}

	// Real view height for mobile devices
	if (window.matchMedia("(max-width: 767px)").matches) {
		$('#intro').css({ height: $(window).height() });
	}

	// Initiate the wowjs animation library
	new WOW().init();

	// Initialize Venobox
	$('.venobox').venobox({
		bgcolor: '',
		overlayColor: 'rgba(6, 12, 34, 0.85)',
		closeBackground: '',
		closeColor: '#fff'
	});

	// Initiate superfish on nav menu
	$('.nav-menu').superfish({
		animation: {
			opacity: 'show'
		},
		speed: 400
	});

	// Mobile Navigation
	if ($('#nav-menu-container').length) {
		var $mobile_nav = $('#nav-menu-container').clone().prop({
			id: 'mobile-nav'
		});
		$mobile_nav.find('> ul').attr({
			'class': '',
			'id': ''
		});
		$('body').append($mobile_nav);
		$('body').prepend('<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>');
		$('body').append('<div id="mobile-body-overly"></div>');
		$('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-chevron-down"></i>');

		$(document).on('click', '.menu-has-children i', function(e) {
			$(this).next().toggleClass('menu-item-active');
			$(this).nextAll('ul').eq(0).slideToggle();
			$(this).toggleClass("fa-chevron-up fa-chevron-down");
		});

		$(document).on('click', '#mobile-nav-toggle', function(e) {
			$('body').toggleClass('mobile-nav-active');
			$('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
			$('#mobile-body-overly').toggle();
		});

		$(document).click(function(e) {
			var container = $("#mobile-nav, #mobile-nav-toggle");
			if (!container.is(e.target) && container.has(e.target).length === 0) {
				if ($('body').hasClass('mobile-nav-active')) {
					$('body').removeClass('mobile-nav-active');
					$('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
					$('#mobile-body-overly').fadeOut();
				}
			}
		});
	} else if ($("#mobile-nav, #mobile-nav-toggle").length) {
		$("#mobile-nav, #mobile-nav-toggle").hide();
	}

	// Smooth scroll for the menu and links with .scrollto classes
	$('.nav-menu a, #mobile-nav a, .scrollto').on('click', function() {
		if (this.hash.indexOf('!') !== -1) {
			return;
		}
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			
			var target = $(this.hash);
			if (target.length) {
				var top_space = 0;

				if ($('#header').length) {
					top_space = $('#header').outerHeight();

					if( ! $('#header').hasClass('header-fixed') ) {
						top_space = top_space - 20;
					}
				}

				$('html, body').animate({
					scrollTop: target.offset().top - top_space
				}, 1500, 'easeInOutExpo');

				if ($(this).parents('.nav-menu').length) {
					$('.nav-menu .menu-active').removeClass('menu-active');
					$(this).closest('li').addClass('menu-active');
				}

				if ($('body').hasClass('mobile-nav-active')) {
					$('body').removeClass('mobile-nav-active');
					$('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
					$('#mobile-body-overly').fadeOut();
				}
				return false;
			}
		}
	});

	// Gallery carousel (uses the Owl Carousel library)
	$(".gallery-carousel").owlCarousel({
		autoplay: true,
		dots: true,
		loop: true,
		center:true,
		responsive: { 0: { items: 1 }, 768: { items: 3 }, 992: { items: 4 }, 1200: {items: 5}
		}
	});

	// Buy tickets select the ticket type on click
	$('#buy-ticket-modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var ticketType = button.data('ticket-type');
		var modal = $(this);
		modal.find('#ticket-type').val(ticketType);
	})

// custom code

});
